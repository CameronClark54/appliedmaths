﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace AppliedMathsAssessment
{
    public class PhysicsObject
    {
        // ------------------
        // Data
        // ------------------

        // Transform
        protected Vector3 rotation;
        protected Vector3 position;
        protected Vector3 scale = Vector3.One;

        // Physics
        protected BoundingBox hitBox;
        protected Vector3 velocity;
        protected Vector3 acceleration;
        protected float drag = 0.05f;
        protected Vector3 collisionScale = Vector3.One;
        protected bool isStatic = false;    // Not moved by physics
        protected bool useGravity = false;  // Falls with gravity each frame
        protected bool isTrigger = true;    // Does not trigger physics affects (but can still be sensed with collisions)
        protected float gravityScale = 1f;

        // Previous state
        protected Vector3 positionPrev;
        protected Vector3 velocityPrev;
        protected Vector3 accelerationPrev;

        // Numerical Integration
        public enum IntegrationMethod {
            EXPLICIT_EULER,
            METHOD_TWO,
            METHOD_THREE
        };
        // UPDATE THIS TO TEST TASK 6
        private IntegrationMethod currentIntegrationMethod = IntegrationMethod.METHOD_THREE;

        // ------------------
        // Behaviour
        // ------------------
        public BoundingBox GetHitBox()
        {
            return hitBox;
        }
        // ------------------
        public virtual void UpdateHitBox()
        {
            // Just make a cube hitbox based on the scale
            hitBox = new BoundingBox(-collisionScale * 0.5f, collisionScale * 0.5f);

            // Move to correct position in game world
            hitBox.Min += position;
            hitBox.Max += position;
        }
        // ------------------
        public Vector3 GetPosition()
        {
            return position;
        }
        // ------------------
        public void SetPosition(Vector3 newPosition)
        {
            position = newPosition;
        }
        // ------------------
        public void SetScale(Vector3 newScale)
        {
            scale = newScale;
        }
        // ------------------
        public Vector3 GetGravityVector()
        {
            return new Vector3(0, -9.8f * gravityScale, 0);
        }
        // ------------------
        public virtual void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Update acceleration due to gravity
            if (useGravity)
                acceleration.Y = -9.8f * gravityScale;

            // Store current state before making any modifications
            Vector3 positionCur = position;
            Vector3 velocityCur = velocity;
            Vector3 accelerationCur = acceleration;

            // Update velocity due to drag
            velocity *= (1.0f - drag);

            // Update velocity and position based on acceleration
            // Uses numerical integration (multiple possible methods)
            switch (currentIntegrationMethod)
            {
                case IntegrationMethod.EXPLICIT_EULER:
                    // This method is being deprecated due to stability issues.
                    position += velocity * dt;
                    velocity += acceleration * dt;
                    break;
                    
                ///////////////////////////////////////////////////////////////////
                //
                // CODE FOR TASK 6 SHOULD BE ENTERED HERE
                //
                ///////////////////////////////////////////////////////////////////
                case IntegrationMethod.METHOD_TWO:

                    // Insert method two code here
                    // Change value of currentIntegrationMethod to test this method
                    velocity += accelerationCur * dt;
                    position += velocityCur * dt;

                    break;

                case IntegrationMethod.METHOD_THREE:

                    // Insert method three code here
                    // Change value of currentIntegrationMethod to test this method
                    velocity += acceleration * dt;
                    position += velocityCur * dt;

                    break;

                ///////////////////////////////////////////////////////////////////  
                // END TASK 6 CODE
                ///////////////////////////////////////////////////////////////////  
            }

            // Store current state as previous state
            positionPrev = positionCur;
            velocityPrev = velocityCur;
            accelerationPrev = accelerationCur;

            // Update hitbox
            UpdateHitBox();
        }
        // ------------------
        public virtual void HandleCollision(PhysicsObject other)
        {
            // Don't react with physics if this object is static
            if (isStatic || isTrigger || other.isTrigger)
                return;

            ///////////////////////////////////////////////////////////////////
            //
            // CODE FOR TASK 3 SHOULD BE ENTERED HERE
            //
            ///////////////////////////////////////////////////////////////////

            //collision depth
            Vector3 centreOther = other.hitBox.Min + (other.hitBox.Min - other.hitBox.Min) / 2.0f;
            Vector3 centreThis = hitBox.Min + (hitBox.Min - hitBox.Min) / 2.0f;

            Vector3 distance = centreOther - centreThis;

            //min distance for no interaction
            Vector3 minDistance = (other.hitBox.Max - other.hitBox.Max) / 2.0f + (hitBox.Max - hitBox.Max) / 2.0f;

            if (distance.X < 0)
            {
                minDistance.X = -minDistance.X;
            }
            if (distance.Y < 0)
            {
                minDistance.Y = -minDistance.Y;
            }
            if (distance.Z < 0)
            {
                minDistance.Z = -minDistance.Z;
            }

            Vector3 depth = minDistance - distance;

            Vector3[] corners = other.hitBox.GetCorners();

            Vector3 planeVector1;
            Vector3 planeVector2;

            if (Math.Abs(depth.Z) > Math.Abs(depth.X))
            {
                //x is smaller so collide on x axis

                //two vectors from east plane
                planeVector1 = corners[0] - corners[5];
                planeVector2 = corners[6] - corners[5];

            }
            else
            {
                //z is smaller collide on z axis

                //two vectors from north plane
                planeVector1 = corners[1] - corners[0];
                planeVector2 = corners[3] - corners[0];
            }

            //get normal vector using cross product
            Vector3 normal = Vector3.Cross(planeVector1, planeVector2);

            //use .normalize to change vector3 to unit vector
            normal.Normalize();

            //reflect the player off the surface
            velocity = Vector3.Reflect(velocity, normal);


            ///////////////////////////////////////////////////////////////////  
            // END TASK 3 CODE
            ///////////////////////////////////////////////////////////////////  
        }
        // ------------------
        public virtual void Draw(Camera cam, DirectionalLightSource light)
        {

        }
        // ------------------
    }
}
